# Projet Pilaf (Février 2020 - Juin 2020)

_Ce projet scolaire est un site vitrine d'un ERP (Enterprise Resource Planning) pour une entreprise fictive de magasin BIO. Cette application web m'a permis de découvrir les difficultés de réaliser un projet en groupe et de découvrir les véritables attentes d'un client._

## Comment utiliser le projet ?

Pour ce projet, on peut juste aller sur le lien : [http://pilaf-projet-tutore.hadrien-pouzol.fr/](http://pilaf-projet-tutore.hadrien-pouzol.fr/)

## Etat du projet 

Fini

## Langages

**FrontEnd :** HTML / CSS

## Auteurs

**Chef de projet :** Hadrien POUZOL

**FrontEnd :** Hadrien POUZOL / Maxime CONSTANS / Xiangyu AN / Adame DAMOUH / Théo DELOBELLE
